import { Component, OnInit } from '@angular/core';
import {Title} from './books';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

    bookArray: Array<Title>;
    constructor() {
        this.bookArray = [];
    }

    addTitle(title, author, isbn, descrip) {
        const titles = new Title(title, author, isbn, descrip);
        this.bookArray.push(titles);
        console.log(this.bookArray);
    }

    removeTitle(book) {
        const index = this.bookArray.indexOf(book);
        this.bookArray.splice(index , 1);
    }

    update(book) {
        let updatedBook;
        let updatedBookName = prompt('Please enter the book name:', book.title);
        let updatedAuthorName = prompt('Please enter the author name:', book.author);
        let updatedIsbnNumber = prompt('Please enter the isbn number:', book.isbn);
        let updatedBookDescrip = prompt('Please enter the book description:', book.descrip);
        updatedBook = new Title(updatedBookName, updatedAuthorName, updatedIsbnNumber, updatedBookDescrip);
        const index = this.bookArray.indexOf(book);
        this.bookArray.splice(index, 1, updatedBook);

        if (updatedBookName == null || updatedBookName === '') {
            updatedBookName = prompt('Please enter the book name:');
            updatedBook = new Title(updatedBookName, updatedAuthorName, updatedIsbnNumber, updatedBookDescrip);
            const index1 = this.bookArray.indexOf(book);
            this.bookArray.splice(index1, 1, updatedBook);
          }
        if (updatedAuthorName == null || updatedAuthorName === '') {
            updatedAuthorName = prompt('Please enter the author name:');
            updatedBook = new Title(updatedBookName, updatedAuthorName, updatedIsbnNumber, updatedBookDescrip);
            const index2 = this.bookArray.indexOf(book);
            this.bookArray.splice(index2, 1, updatedBook);
          }
        if (updatedIsbnNumber == null || updatedIsbnNumber === '') {
            updatedIsbnNumber = prompt('Please enter the isbn number:');
            updatedBook = new Title(updatedBookName, updatedAuthorName, updatedIsbnNumber, updatedBookDescrip);
            const index3 = this.bookArray.indexOf(book);
            this.bookArray.splice(index3, 1, updatedBook);
          }
        if (updatedBookDescrip == null || updatedBookDescrip === '') {
            updatedBookDescrip = prompt('Please enter the description:');
            updatedBook = new Title(updatedBookName, updatedAuthorName, updatedIsbnNumber, updatedBookDescrip);
            const index4 = this.bookArray.indexOf(book);
            this.bookArray.splice(index4, 1, updatedBook);
          }
    }

}

