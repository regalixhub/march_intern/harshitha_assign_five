export class Title{

    title: string[];
    author: string[];
    isbn: number[];
    descrip: string[];

    constructor(title, author, isbn, descrip){
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.descrip = descrip;

    }
}